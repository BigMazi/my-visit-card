import Vue from 'vue'
import VueRouter from 'vue-router'
import MyPhotos from '../components/MyPhotos.vue'


Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: MyPhotos
    },
    {
        path: '/history',
        name: 'History',
        component: () => import('../components/MyStory.vue')
    },
    {
        path: '/code',
        name: 'Code',
        component: () => import('../components/GitLabLink.vue')
    },
    {
        path: '/video',
        name: 'Video',
        component: () => import('../components/Video.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
